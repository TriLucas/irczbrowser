// ==UserScript==
// @name         IRCZ AutoRandom
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  Automatically, randomly browser ircz pages.
// @author       Sheep
// @match        https://ircz.de/p/*
// @match        https://9gag.com/*
// @icon         https://www.google.com/s2/favicons?domain=ircz.de
// @grant        none
// ==/UserScript==

"use strict";

const DEFAULT_TIMER = 5000;

(function()
{
  let controlsCss = "" +
    "#controls-menu {\n" +
    "display: block;\n" +
    "position: fixed;\n" +
    "top: 50px;\n" +
    "left: 10px;\n" +
    "background-color: white;\n" +
    "opacity: 0.2;\n" +
    "border: 1px dashed black;\n" +
    "}\n" +
    "\n" +
    "#controls-menu:hover {\n" +
    "opacity: 1;\n" +
    "}\n" +
    "#inputToggle {\n" +
    "width: 100%;\n" +
    "}\n" +
    "#inputToggle[value=\"paused\"]{\n" +
    "background-color: red;\n" +
    "}\n" +
    "#inputToggle[value=\"started\"]{\n" +
    "background-color: green;\n" +
    "}\n" +
    "#inputInterval {\n" +
    "width: 60px;\n" +
    "}\n" +
    ".sec-label {\n" +
    "padding: 0px 3px 0px 3px;\n" +
    "}\n";
  let boolStr = function(value)
  {
    if (typeof value === "undefined")
      return false;
    if (value === "true")
      return true;
    return false;
  };

  let createElement = function(tag, attrs = {})
  {
    let res = document.createElement(tag);
    for (let key of Object.keys(attrs))
    {
      res.setAttribute(key, attrs[key]);
    }
    return res;
  };

  let randomizerIRCZ = function()
  {
    document.getElementById("a-random").click();
  };

  let randomizer9GAG = function()
  {
    document.location = "/shuffle";
  };

  let matchMapping = {
    "^https://9gag.com/gag/.*$": randomizer9GAG,
    "^https://ircz.de/p/.*$": randomizerIRCZ
  };

  let cookieValue = class
  {
    constructor(parent, parser, defaultValue, cookieName, id)
    {
      this._parent = parent;
      this._value = void 0;
      this.parser = parser;
      this.defaultValue = defaultValue;
      this.cookieName = cookieName;
      this.inputAttrs = {id};
      this.element = void 0;
    }

    _readFromCookie()
    {
      let cookieParts = decodeURIComponent(document.cookie).split(";");
      let cname = this.cookieName + "=";
      let i = 0;
      for (i = 0; i < cookieParts.length; i++)
      {
        let c = cookieParts[i];
        while (c.charAt(0) == " ")
        {
          c = c.substring(1);
        }
        if (c.indexOf(cname) == 0)
        {
          this._value = this.parser(c.substring(cname.length, c.length));
          return;
        }
      }
      this._value = this.defaultValue;
    }

    _writeToCookie()
    {
      let exdays = 10;
      let d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

      let expires = "expires=" + d.toUTCString();
      let actualCookie = this.cookieName + "=" + this.value + ";";
      document.cookie = actualCookie + expires + ";path=/";
    }

    get value()
    {
      if (typeof this._value === "undefined")
      {
        this._readFromCookie();
      }
      return this._value;
    }

    set value(v)
    {
      this._value = v;
      this._writeToCookie();
      this.update();
    }

    update()
    {
      this._parent.update();
    }

    addHandlers() {}

    getInputAttrs()
    {
      let res = this.inputAttrs;
      res.value = this.displayedValue();
      return res;
    }

    generateHtml()
    {
      this.element = createElement("input", this.getInputAttrs());
      this.addHandlers();
      return this.element;
    }

    displayedValue()
    {
      return this.value;
    }
  };

  let ButtonBooleanCookie = class extends cookieValue
  {
    constructor(parent, defaultValue, cookieName, id)
    {
      super(parent, boolStr, defaultValue, cookieName, id);
      this.inputAttrs.type = "button";
    }

    update()
    {
      this.element.setAttribute("value", this.displayedValue());
      super.update();
    }

    addHandlers()
    {
      super.addHandlers();
      let self = this;
      this.element.onclick = function()
      {
        self.value = !self.value;
      };
    }

    displayedValue()
    {
      return super.displayedValue() ? "started" : "paused";
    }
  };

  let TimerIntervalCookie = class extends cookieValue
  {
    constructor(parent, defaultValue, cookieName, id)
    {
      super(parent, parseInt, defaultValue, cookieName, id);
      this._keyupInterval = void 0;
      this.inputAttrs.type = "text";
    }

    displayedValue()
    {
      return Math.floor(super.displayedValue() / 1000);
    }

    addHandlers()
    {
      super.addHandlers();
      this.element.onkeyup = this.getOnKeyUp();
    }

    readValue()
    {
      let targetValue = this.parser(this.element.value) * 1000;
      if (!(Number.isNaN(targetValue)) && targetValue != this.value)
      {
        this.value = targetValue;
      }
    }

    generateHtml()
    {
      let elem = super.generateHtml();
      let wrapper = createElement("span", {class: "sec-wrapper"});
      let label = createElement("span", {class: "sec-label"});
      label.innerHTML = "sec";
      wrapper.appendChild(elem);
      wrapper.appendChild(label);
      return wrapper;
    }

    getOnKeyUp()
    {
      let self = this;
      return function()
      {
        if (!(typeof self._keyupInterval === "undefined"))
        {
          window.clearTimeout(self._keyupInterval);
        }

        self._keyupInterval = window.setTimeout(() =>
        {
          self._keyupInterval = void 0;
          self.readValue();
        }, 300);
      };
    }
  };

  let autorunner = class
  {
    constructor()
    {
      this.intervalId = void 0;
      this.autoStarted = void 0;
      this.autoInterval = void 0;
    }

    show()
    {
      this.autoStarted = new ButtonBooleanCookie(this, false, "autoStarted",
                                                 "inputToggle");
      this.autoInterval = new TimerIntervalCookie(this, DEFAULT_TIMER,
                                                  "autoInterval",
                                                  "inputInterval");

      let css = createElement("style", {type: "text/css"});
      css.innerHTML = controlsCss;
      let menu = createElement("div", {id: "controls-menu"});
      let header = createElement("div", {id: "headerWrapper"});
      header.innerHTML = "autoRandom status:";

      document.body.appendChild(css);
      menu.appendChild(header);
      menu.appendChild(this.autoInterval.generateHtml());
      menu.appendChild(createElement("br"));
      menu.appendChild(this.autoStarted.generateHtml());
      document.body.appendChild(menu);
    }

    update()
    {
      if (!(typeof this.intervalId === "undefined"))
      {
        window.clearTimeout(this.intervalId);
      }

      if (this.autoStarted.value)
      {
        for (let key of Object.keys(matchMapping))
        {
          let regex = new RegExp(key);
          if (!(document.location.href.match(regex) == null))
          {
            let executor = matchMapping[key];
            this.intervalId = window.setTimeout(executor,
                                                this.autoInterval.value);
            break;
          }
        }
      }
    }
  };

  let runner = new autorunner();
  runner.show();
  runner.update();
})();
